
# YAWA (Yet Another Waze Application)

Esta es una aplicación que busca simular un comportamiento similar a la aplicación Waze, teniendo algunas funcionalidades similares
como poder ver la posición actual de la  persona, y poder ver un historial de estas posiciones, la aplicación también implementa
un sistema de cuenta, para diferenciar a los diferentes usuarios y conservar los datos por cuenta. Basada en la aplicación
base dearrollada en railstutorial.org, la aplicación esta alojada en:
https://yawa123.herokuapp.com/

## Instalación y deployment


### Prerequisitos

Ruby 2.4.1,
recomendación recomendada con RVM
https://rvm.io/rvm/install
ejm en ubuntu:
```
sudo apt-get install software-properties-common
```
```
sudo apt-add-repository -y ppa:rael-gc/rvm
sudo apt-get update
sudo apt-get install rvm
```
cerrar y abrir una nueva terminal
```
rvm install ruby
```

Instalar rails
Ruby on Rails -v 5.1.4
```
gem install rails -v 5.1.4
```

Base de datos:
Desarollo: Sqlite3
Test y producción: Postgres

En caso de querer hacer deploy, se necesita instalar heroku, esto en la sección de deployment.

### Instalación

Una vez que se haga clone/pull al repositorio, sera necesario realizar instalar las gemas necesarias,
en caso de ser para producción, o testing en DCA, se necesita instalar previamente postgres, y se elimina el flag del comando

```
$ bundle install --without production
```

luego migramos la basde datos

```
$ rails db:migrate
```
Y la aplicación estará lista para ejecución local.
```
$ rails server
```


## Deployment

Se asume se tiene ya un repositorio previo en github/bitbucket para realizar el deploy, y una cuenta de heroku (https://signup.heroku.com/).

Es necesario instalar heroku previamente para el deploy, se puede verificar si esta instalado con:
```
heroku --version
```
en caso de que no, se deben seguir los siguiente pasos segun sistema operativo, presentes en la pagina de heroku.
https://devcenter.heroku.com/articles/heroku-cli

una vez instalado, se accede a la cuenta de heroku desde terminal

```
heroku login
heroku keys:add
```
luego se usa el comando heroku create, el cual crea un subdominio para la aplicación, este sera la url para poder ver la aplicación, estará disponible inmediatamente.
```
heroku create
```
ahora se integra heroku con el repositorio de github control
```
git push heroku master
```
y listo, la aplicación estará disponible en el link dado por heroku create. Opcionalmente, si se quiere cambiar el dominio a algo mas simple se puede correr
```
heroku rename best-name-ever
```
en cuyo caso, si el dominio esta disponible, el nuevo dominio de la aplicación será best-name-ever.herokuapp.com

url de esta aplicación
https://yawa123.herokuapp.com/

## Autor

Juan Fernando Rincón


## Acknowledgments

* railstutorial.org , del cual se basa la applicación
* stackoverflow
